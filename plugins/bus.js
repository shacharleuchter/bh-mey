import Vue from 'vue'
const eventBus = {}

eventBus.install = (Vue) => {
  Vue.prototype.$bus = new Vue()
}

Vue.use(eventBus)

/**
Avilable events
---------------
preloaded (string payload: audio, video) (listener on index)
endPreloader (hides preloader)
videoSeekNextFrame (video: next frame)
videoRestart (video: restart to timeline.start)
videoPlaying (Boolean) (listener on AudioPlayer)
introVideoFinished
nextLevel (listener on index)
nextScene (listener on index)
touchTimerFinished
triggerTouchStart
triggerTouchEnd
startIntroVideo
startIntro
startGame
*/

/*
Broadcast
---------
this.$bus.$emit('eventName', payload)
*/

/*
Listen
------
this.$bus.$on('eventName', this.handlerFunction)

// only listen for the first event
this.$bus.$once('eventName', this.handlerFunction)

// remove listner
this.$bus.$off('eventName', this.handlerFunction)
*/
