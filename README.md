# bh-mey

> Mey BH Game

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
# service worker is disabled in dev
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Plugins Docs

https://github.com/dobromir-hristov/vue-vimeo-player

https://github.com/vimeo/player.js

https://kazupon.github.io/vue-i18n/en/
