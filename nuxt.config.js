const resolve = require('path').resolve
module.exports = {
  /*
  ** Build configuration
  */
  build: {
    vendor: [
      'howler'
    ]
  },
  plugins: [
    { src: '~plugins/bus' }
  ],
  css: [
    // SCSS file in the project
    '@/assets/css/global.scss'
  ],
  loadingIndicator: {
    name: 'pulse',
    color: '#c7513d',
    background: 'black'
  },
  /*
  ** Headers
  ** Common headers are already provided by @nuxtjs/pwa preset
  */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, height=device-height, initial-scale=1, user-scalable=no' },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { hid: 'description', name: 'description', content: 'Meta description' }
    ],
    link: [
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Customize app manifest
  */
  manifest: {
    theme_color: '#3B8070',
    display: 'fullscreen',
    orientation: 'landscape'
  },
  /*
  ** Modules
  */
  modules: [
    '@nuxtjs/pwa',
    'nuxt-sass-resources-loader'
  ],
  sassResources: [
    resolve(__dirname, 'assets/css/global.scss')
  ]
}
