const fs = require('fs')
const semver = require('semver')
const getPackageJsonPath = () => `${process.cwd()}/package.json`
const packageJson = require(getPackageJsonPath())
const writePackageJson = configObject =>
    fs.writeFileSync(getPackageJsonPath(),
        `${JSON.stringify(configObject, null, 2)}\n`)
const oldVersion = packageJson.version
let newStableVersion

function bumpVersion (releaseType) {
    // Tag a new release
  newStableVersion = packageJson.version = semver.inc(oldVersion, releaseType)
  writePackageJson(packageJson)
  console.log(`Version bumped from ${oldVersion} to ${newStableVersion}`)
}

function getNewVersion () {
  return newStableVersion
}

function getOldVersion () {
  return oldVersion
}

module.exports = {
  bumpVersion: bumpVersion,
  oldVersion: getOldVersion,
  newVersion: getNewVersion
}
