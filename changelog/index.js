/**
 * TODO:
 * - Check if last commit has already been released and abort
 * - Check if we have a changelog.md file in the docs folder otherwise write one
 * - add more dynamic vars to options object
 * - fix ignore filtering
 */

/**
 * README
 * What is this script doing?
 * --
 * Updates the release version and writes all commits to changelog.
 * -
 * How is it doing it?
 * --
 * 1) Gets the last release tag and scans all commits from the git HEAD to that point.
 * 2) From the commits determines what type of release it should be and bumps the package.
 * 3) Formats the commits to markdown and writes to changelog.md.
 * -
 * Release assumptions
 * - Your working on a git-flow structure
 * - You make a release from develop branch
 * - Your release tags are in a clear SemVer format (1.0.0, v1.0.0, =1.0.0)
 * - A Jira ticket is written inside brackets [] and can be located anywhere in the commit
 */
const fs = require('fs')
const Git = require('nodegit')
const semver = require('semver')
const path = require('path')
const bump = require('./bump')
const getPackageJsonPath = () => `${process.cwd()}/package.json`
const packageJson = require(getPackageJsonPath())
const options = {
  jiraUrl: 'https://jira.jvm.de/browse/',
  relTypes: {
    feat: 'Features',
    fix: 'Bug Fixes',
    perf: 'Performance Improvements',
    docs: 'Documentation',
    style: 'Styles',
    refactor: 'Code Refactoring',
    chore: 'Chores'
  }
}

let releaseVars = {
  pathToChangelog: path.resolve('./CHANGELOG.md'),
  gitRepo: packageJson.repository.url,
  pathToRepo: path.resolve('./.git'),
  currentRepo: undefined,
  startCommit: undefined,
  finishCommit: undefined,
  oldVersion: bump.oldVersion(),
  newVersion: undefined,
  releaseType: undefined,
  releaseCommits: [],
  regex: {
    commitRegex: /^(\w*)(?:\((.*)\))?\:(.*)$/gm,
    ticketRegex: /\[(.*?)\]/gmi,
    majorRegex: /\b(BREAKING CHANGE|BREAKING CHANGES|MAJOR RELEASE)\b/
  }
}

let getStartCommit = function (repo) {
  releaseVars.currentRepo = repo
  return repo.getHeadCommit()
        .then(function (commit) {
            // get the HEAD commit of the current branch and save it as the starting commit
          releaseVars.startCommit = commit
            // console.log('headCommit.startCommit', releaseVars.startCommit.sha());
          return repo
        })
}

let getTagList = function (repo) {
  return Git.Tag.list(repo).then(function (list) {
        // console.log('tagList', list);
    if (list.length === 0) {
      releaseVars.finishCommit = undefined
      return scanCommits({ startCommit: releaseVars.startCommit, finishCommit: releaseVars.finishCommit })
    } else {
            // first lets filter all tags which are not valid SemVer
            // TODO: optionaly sort the array
      list = list.filter(tagName => semver.valid(tagName) !== null)
            // console.log('tagList filterd', list, list[0]);
      return list
    }
  })
}

let getTagOid = function (list) {
    // get latest tag name Oid
  return Git.Reference.nameToId(releaseVars.currentRepo, 'refs/tags/' + list[0])
        .then(function (tagRef) {
            // console.log('tagRef', tagRef);
          return getFinishCommit(tagRef)
        })
}

let getFinishCommit = function (tagRef) {
  return Git.Commit.lookup(releaseVars.currentRepo, tagRef).then(function (commit) {
        // save last release commit Oid as finishCommit
    releaseVars.finishCommit = commit.sha()
        // console.log('finishCommit', releaseVars.finishCommit);
    return scanCommits({ startCommit: releaseVars.startCommit, finishCommit: releaseVars.finishCommit })
  })
}

/**
 * Init nodegit to kickoff everything
 * First getStartCommit then getTagList
 * If we find tags getTagOid -> getFinishCommit -> scanCommits -> processCommits
 * Otherwise scanCommits -> processCommits
 */
Git.Repository.open(releaseVars.pathToRepo)
    .then(getStartCommit)
    .then(getTagList)
    .done(getTagOid)

function scanCommits (args) {
  // now lets run history from startCommit
  let history = args.startCommit.history()

  // Create a counter to only show up to the last found commit.
  let stopCount = false

  history.on('commit', function (commit) {
    if (args.finishCommit) {
      if (commit.sha() === args.finishCommit) {
        stopCount = true
      }
    }
    if (stopCount) {
            // ignore commits older then finishCommit
      return
    } else {
      let commitObj = {
        id: commit.toString().substring(0, 7),
        sha: commit.sha(),
        message: commit.message(),
        body: commit.body(),
        header: commit.summary(),
        date: commit.date(),
        author: commit.author(),
        convention: prepareConvention(commit.summary(), releaseVars.regex.commitRegex),
        isMajor: commit.body() ? commit.body().match(releaseVars.regex.majorRegex) !== null : false,
        jiraTicket: checkJiraTicket(commit.message()),
        url: commitUrl(commit.toString().substring(0, 7))
      }
            // here we include only commits that follow convention
      if (commitObj.convention) {
        releaseVars.releaseCommits.push(commitObj)
      }
    }
  })

  history.on('end', function () {
        // finished reading the commits. time to make it rain
    processCommits(releaseVars.releaseCommits)
  })

  history.start()
}

function commitUrl (commit) {
  if (releaseVars.gitRepo) {
    return releaseVars.gitRepo + '/commits/' + commit
  }
}

function compareUrl (oldVer, newVer) {
  if (releaseVars.gitRepo) {
    return releaseVars.gitRepo + '/compare/commits?targetBranch=refs%2Ftags%2F' + oldVer + '&sourceBranch=refs%2Ftags%2F' + newVer
  }
}

function jiraTicketUrl (id) {
  if (id) {
    return options.jiraUrl + id.substr(1).slice(0, -1)
  }
}

function checkJiraTicket (text) {
  if (!text) return
  let m = text.match(releaseVars.regex.ticketRegex)

  return m === null ? undefined : { name: m[0], url: jiraTicketUrl(m[0]) }
}

function getReleaseType (commits) {
  const isMinor = commits.filter(commit => commit.convention.type === 'feat').length > 0
  const isMajor = commits.filter(commit => commit.isMajor === true).length > 0

  if (isMinor) {
    releaseVars.releaseType = 'minor'
  } else if (isMajor) {
    releaseVars.releaseType = 'major'
  } else {
    releaseVars.releaseType = 'patch'
  }

  return releaseVars.releaseType
}

function renameType (type) {
  let commitType = options.relTypes[type]
  if (!commitType) return type
  return commitType
}

function prepareConvention (commit, regex) {
  let m
  let convention
  while ((m = regex.exec(commit)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++
    }

        // TODO: perhaps some checking for errors should come here just in case we are not linting commits
    convention = {
      type: m[1],
      scope: m[2],
      description: m[3]
    }
  }
  return convention
}

function formatCommitsToMarkdown (commits) {
  return commits.map(function (commit) {
    let jiraTicket = commit.jiraTicket ? ' [' + commit.jiraTicket.name + '(' + commit.jiraTicket.url + ')]' : ''
    let author = commit.author ? ' _by [' + commit.author.name() + '](' + commit.author.email() + ')_' : ''
    return '* **' + commit.convention.scope + ':** ' + commit.convention.description + ' ([' + commit.id + ']' + '(' + commit.url + '))' + jiraTicket + author + '\n'
  })
}

function formattedDate (d = new Date()) {
  let month = String(d.getMonth() + 1)
  let day = String(d.getDate())
  const year = String(d.getFullYear())

  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day

  return `${day}.${month}.${year}`
}

function prepareMarkdownTemplate (groups) {
  let today = formattedDate()
    // here we preper the release header
  const releaseHeader = `# [${bump.newVersion()}](${compareUrl(bump.oldVersion(), bump.newVersion())}) (${today})\n`
    // here we structure the type groups and commits and convert to string
  const commitGroups = groups.map(function (group) {
    return '### ' + group.type + '\n\n' + formatCommitsToMarkdown(group.commits).join('')
  }).join('')
  return releaseHeader + commitGroups
}

function processCommits (commits) {
  let groups = []
    // get commit types
  let types = commits
        // ignore these commit types
        .filter(commit => commit.convention.type !== ('build' || 'revert' || 'test'))
        // extract commit types for grouping
        .map(commit => commit.convention.type)
    // extract unique and sort the types
  let uniqueAndSorted = [...new Set(types)].sort()

  uniqueAndSorted.forEach(function (type) {
    groups.push({
      type: renameType(type),
      commits: commits.filter(commit => commit.convention.type === type)
    })
  })

    // check for release type and save it to releaseType
  getReleaseType(commits)

    // bump version in package
  bump.bumpVersion(releaseVars.releaseType)

    // now convert commits to markdown format and save to disk
  writeChangelog(prepareMarkdownTemplate(groups))
}

function writeChangelog (commits) {
  // Write changelog to file
  // read existing contents into data
  var data = fs.readFileSync(releaseVars.pathToChangelog)
  var fd = fs.openSync(releaseVars.pathToChangelog, 'w+')
  var buffer = new Buffer(commits)
  // write new data
  fs.writeSync(fd, buffer, 0, buffer.length, 0)
  // append old data
  fs.writeSync(fd, data, 0, data.length, buffer.length)

  fs.close(fd)

  console.log(`
----------- ${releaseVars.releaseCommits.length} commits added to CHANGELOG -------------

Next steps:
***********

1) Commit files with message:
   Prepare release ${bump.newVersion()}
2) Do git-flow release start, git-flow release finish
3) Tag new version: "${bump.newVersion()}" and publish to origin
4) Push everything
5) Run Deploy script for both dev and stage
6) Take 5 minutes break ;-)

------------------------------------------------------`)
}
