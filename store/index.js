export const state = () => ({
  game: {
    preloaded: false,
    introFinished: false,
    levels: undefined,
    currentLevel: 'intro',
    levelConfig: undefined,
    levelScenes: undefined,
    currentScene: 0, // ['intro', 'touch, 'end', 'fail']
    language: 'de',
    difficulty: 'normal',
    landscapeGuerdActive: false,
    pauseMenuActive: false,
    isMobile: false
  },
  media: {
    mute: false,
    playSoundEffect: ''
  }
})

export const mutations = {
  setMobileView (state) {
    state.game.isMobile = true
  },
  preloaded (state) {
    state.game.preloaded = true
  },
  currentScene (state, payload) {
    state.game.currentScene = payload
  },
  setDifficulty (state, payload) {
    state.game.difficulty = payload
  },
  setLevels (state, payload) {
    state.game.levels = payload
  },
  setLevelScenes (state, payload) {
    state.game.levelScenes = payload
  },
  currentLevel (state, payload) {
    state.game.currentLevel = payload
  },
  setLevelConfig (state, payload) {
    state.game.levelConfig = payload
  },
  introFinished (state, payload) {
    state.game.introFinished = payload
  },
  triggerLandscapeGuerd (state, payload) {
    state.game.landscapeGuerdActive = payload
  },
  triggerPauseMenu (state, payload) {
    state.game.pauseMenuActive = payload
  },
  language (state, payload) {
    state.game.language = payload
  },
  playSoundEffect (state, payload) {
    state.media.playSoundEffect = payload
  }
}

export const actions = {
  setDifficulty: function ({ commit }, payload) {
    commit('setDifficulty', payload)
  },
  setPreloaded: function ({ commit }) {
    commit('preloaded')
  },
  setMobileView: function ({ commit }) {
    commit('setMobileView')
  },
  setIntroFinished: function ({ commit }, payload) {
    commit('introFinished', payload)
  },
  setScene: function ({ commit }, payload) {
    commit('currentScene', payload)
  },
  setLevel: function ({ commit }, payload) {
    commit('currentLevel', payload)
  },
  setLevelConfig: function ({ commit }, payload) {
    commit('setLevelConfig', payload)
  },
  setLevels: function ({ commit }, payload) {
    commit('setLevels', payload)
  },
  setLevelScenes: function ({ commit }, payload) {
    commit('setLevelScenes', payload)
  },
  triggerLandscapeGuerd: function ({ commit }, payload) {
    commit('triggerLandscapeGuerd', payload)
  },
  triggerPauseMenu: function ({ commit }, payload) {
    commit('triggerPauseMenu', payload)
  },
  setLanguage: function ({ commit }, payload) {
    commit('language', payload)
  },
  playSoundEffect: function ({ commit }, payload) {
    commit('playSoundEffect', payload)
  }
}

export const getters = {
  difficulty (state) {
    return state.game.difficulty
  },
  levelConfig (state) {
    return state.game.levelConfig
  },
  isMobile (state) {
    return state.game.isMobile
  },
  showLandscapeGuerd (state) {
    return state.game.landscapeGuerdActive
  },
  showPauseMenu (state) {
    return state.game.pauseMenuActive
  },
  introFinished (state) {
    return state.game.introFinished
  },
  gamePreloaded (state) {
    return state.game.preloaded
  },
  levels (state) {
    return state.game.levels
  },
  levelScenes (state) {
    return state.game.levelScenes
  },
  currentLevel (state) {
    return state.game.currentLevel
  },
  currentScene (state) {
    return state.game.currentScene
  },
  language (state) {
    return state.game.language
  },
  getSoundEffect (state) {
    return state.media.playSoundEffect
  }
}
