export default {
  game: {
    intro: {
      start: 0,
      finish: 3,
      text: {
        gameTitle: {
          en: 'BRA PRO',
          de: 'BRA PRO'
        },
        gameOptionsTitle: {
          en: 'How practiced are you in opening a bra? <br>Choose your difficulty and convince me.',
          de: 'Wie geübt bist du im öffnen eines BH’s? <br>Wähle deine Schwierigkeit und überzeuge mich.'
        },
        gameOptions: {
          en: ['TRAINED', 'PRO'],
          de: ['geübt', 'könner']
        },
        gamePlayButton: {
          en: 'LETS TRAIN!',
          de: 'LETS TRAIN!'
        },
        gestureDescription: {
          en: 'Pinch handles<br>to play!',
          de: 'Fingerschnipsen<br>zum Spielen!'
        }
      },
      drag: {
        tolerance: 30,
        artboardSize: { width: 400, height: 212 },
        touchPoints: [
          [
            { x: 65, y: 105.5 },
            { x: 72.9697, y: 105.5 },
            { x: 80.9394, y: 105.5 },
            { x: 88.9091, y: 105.5 },
            { x: 96.8788, y: 105.5 },
            { x: 104.8485, y: 105.5 },
            { x: 112.8182, y: 105.5 },
            { x: 120.7879, y: 105.5 },
            { x: 128.7576, y: 105.5 },
            { x: 136.7273, y: 105.5 },
            { x: 144.697, y: 105.5 },
            { x: 152.6667, y: 105.5 },
            { x: 160.6364, y: 105.5 }
          ],
          [
            { x: 331, y: 106 },
            { x: 323.0303, y: 106 },
            { x: 315.0606, y: 106 },
            { x: 307.0909, y: 106 },
            { x: 299.1212, y: 106 },
            { x: 291.1515, y: 106 },
            { x: 283.1818, y: 106 },
            { x: 275.2121, y: 106 },
            { x: 267.2424, y: 106 },
            { x: 259.2727, y: 106 },
            { x: 251.303, y: 106 },
            { x: 243.3333, y: 106 },
            { x: 235.3636, y: 106 }
          ]
        ]
      }
    },
    levels: [
      {
        // Level 1
        drag: {
          tolerance: 30,
          // The Illustrator artboard size
          artboardSize: { width: 400, height: 212 },
          // Illustrator will export all dots (rectangles actually) as follows:
          // <rect x="62.5" y="92.5" width="1" height="1"/>
          // We will use, the x and y attributes to create the following array of coordinates:
          touchPoints: [
            [
              { x: 62.5, y: 92.5 },
              { x: 68.5099, y: 89.7008 },
              { x: 74.8473, y: 86.84 },
              { x: 81.4866, y: 83.9459 },
              { x: 88.402, y: 81.0472 },
              { x: 95.5677, y: 78.1721 },
              { x: 102.9582, y: 75.3491 },
              { x: 110.5475, y: 72.6067 },
              { x: 118.31, y: 69.9733 },
              { x: 126.2199, y: 67.4774 },
              { x: 134.2516, y: 65.1473 },
              { x: 142.3792, y: 63.0116 },
              { x: 150.577, y: 61.0986 },
              { x: 158.8194, y: 59.4369 },
              { x: 167.0805, y: 58.0548 },
              { x: 175.3346, y: 56.9808 },
              { x: 183.556, y: 56.2434 },
              { x: 191.719, y: 55.8709 },
              { x: 199.7978, y: 55.8918 },
              { x: 207.7666, y: 56.3346 },
              { x: 215.5999, y: 57.2277 },
              { x: 223.2717, y: 58.5995 },
              { x: 230.7564, y: 60.4785 },
              { x: 238.0283, y: 62.8931 },
              { x: 245.0616, y: 65.8717 },
              { x: 251.8305, y: 69.4429 },
              { x: 258.3094, y: 73.635 },
              { x: 264.5, y: 78.5 }
            ],
            [
              { x: 335.5, y: 115.8709 },
              { x: 329.4901, y: 118.6701 },
              { x: 323.1527, y: 121.5309 },
              { x: 316.5134, y: 124.4249 },
              { x: 309.598, y: 127.3237 },
              { x: 302.4323, y: 130.1988 },
              { x: 295.0418, y: 133.0218 },
              { x: 287.4525, y: 135.7642 },
              { x: 279.69, y: 138.3976 },
              { x: 271.7801, y: 140.8935 },
              { x: 263.7484, y: 143.2236 },
              { x: 255.6208, y: 145.3593 },
              { x: 247.423, y: 147.2722 },
              { x: 239.1806, y: 148.934 },
              { x: 230.9195, y: 150.3161 },
              { x: 222.6654, y: 151.3901 },
              { x: 214.444, y: 152.1275 },
              { x: 206.281, y: 152.5 },
              { x: 198.2022, y: 152.4791 },
              { x: 190.2334, y: 152.0363 },
              { x: 182.4001, y: 151.1432 },
              { x: 174.7283, y: 149.7714 },
              { x: 167.2436, y: 147.8924 },
              { x: 159.9717, y: 145.4778 },
              { x: 152.9384, y: 142.4991 },
              { x: 146.1695, y: 138.928 },
              { x: 139.6906, y: 134.7358 },
              { x: 133.5, y: 129.8709 }
            ]
          ]
        },
        timeline: [
          {
            // level intro
            start: 4.1,
            finish: 13.001
          },
          {
            // level touch intro
            start: 13.001,
            finish: 13.001
          },
          {
            // level touch
            start: 14,
            finish: 18
          },
          {
            // level win
            start: 18,
            finish: 23.5
          },
          {
            // level fail
            start: 24.5,
            finish: 28
          }
        ],
        restart: 15,
        text: {
          levelTitle: {
            en: 'Level 1',
            de: 'Level 1'
          },
          levelName: {
            en: 'MORNING GLORY',
            de: 'MORNING GLORY'
          },
          levelIntroTitle: {
            en: 'The Situation',
            de: 'Die Situation'
          },
          levelIntroText: {
            en:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.',
            de:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.'
          }
        }
      },
      {
        // Level 2
        drag: {
          tolerance: 30,
          artboardSize: { width: 400, height: 212 },
          touchPoints: [
            [
              { x: 39.5, y: 73.5 },
              { x: 41.685, y: 63.612 },
              { x: 44.2155, y: 53.785 },
              { x: 47.0834, y: 44.6599 },
              { x: 50.2809, y: 36.8773 },
              { x: 53.7999, y: 31.0781 },
              { x: 57.6325, y: 27.903 },
              { x: 61.7249, y: 27.975 },
              { x: 65.682, y: 31.3383 },
              { x: 69.459, y: 37.379 },
              { x: 73.0968, y: 45.4499 },
              { x: 76.6364, y: 54.9037 },
              { x: 80.1187, y: 65.093 },
              { x: 83.5847, y: 75.3705 },
              { x: 87.0754, y: 85.0889 },
              { x: 90.6318, y: 93.601 },
              { x: 94.2949, y: 100.2593 },
              { x: 98.1056, y: 104.4167 },
              { x: 102.1604, y: 105.4125 },
              { x: 106.0661, y: 102.4438 },
              { x: 109.2528, y: 96.1486 },
              { x: 112.0783, y: 87.6657 },
              { x: 114.9001, y: 78.134 },
              { x: 118.0758, y: 68.6924 },
              { x: 121.9632, y: 60.4795 },
              { x: 126.9197, y: 54.6343 },
              { x: 133.0141, y: 52.3082 },
              { x: 139.023, y: 53.5045 },
              { x: 145.1241, y: 57.2859 },
              { x: 151.1449, y: 62.9893 },
              { x: 156.9128, y: 69.9515 },
              { x: 162.2554, y: 77.5094 },
              { x: 167, y: 85 }
            ],
            [
              { x: 360, y: 72.5 },
              { x: 357.815, y: 62.612 },
              { x: 355.2845, y: 52.785 },
              { x: 352.4166, y: 43.6599 },
              { x: 349.2191, y: 35.8773 },
              { x: 345.7001, y: 30.0781 },
              { x: 341.8675, y: 26.903 },
              { x: 337.7751, y: 26.975 },
              { x: 333.818, y: 30.3383 },
              { x: 330.041, y: 36.379 },
              { x: 326.4032, y: 44.4499 },
              { x: 322.8636, y: 53.9037 },
              { x: 319.3813, y: 64.093 },
              { x: 315.9153, y: 74.3705 },
              { x: 312.4246, y: 84.0889 },
              { x: 308.8682, y: 92.601 },
              { x: 305.2051, y: 99.2593 },
              { x: 301.3944, y: 103.4167 },
              { x: 297.3396, y: 104.4125 },
              { x: 293.4339, y: 101.4438 },
              { x: 290.2472, y: 95.1486 },
              { x: 287.4217, y: 86.6657 },
              { x: 284.5999, y: 77.134 },
              { x: 281.4242, y: 67.6924 },
              { x: 277.5368, y: 59.4795 },
              { x: 272.5803, y: 53.6343 },
              { x: 266.4859, y: 51.3082 },
              { x: 260.477, y: 52.5045 },
              { x: 254.3759, y: 56.2859 },
              { x: 248.3551, y: 61.9893 },
              { x: 242.5872, y: 68.9515 },
              { x: 237.2446, y: 76.5094 },
              { x: 232.5, y: 84 }
            ]
          ]
        },
        timeline: [
          {
            // level intro
            start: 4.1,
            finish: 13.001
          },
          {
            // level touch intro
            start: 13.001,
            finish: 13.001
          },
          {
            // level touch
            start: 14,
            finish: 18
          },
          {
            // level win
            start: 18,
            finish: 23.5
          },
          {
            // level fail
            start: 24.5,
            finish: 28
          }
        ],
        restart: 15,
        text: {
          levelTitle: {
            en: 'Level 2',
            de: 'Level 2'
          },
          levelName: {
            en: 'MORNING GLORY',
            de: 'MORNING GLORY'
          },
          levelIntroTitle: {
            en: 'The Situation',
            de: 'Die Situation'
          },
          levelIntroText: {
            en:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.',
            de:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.'
          }
        }
      },
      {
        // Level 3
        drag: {
          tolerance: 30,
          artboardSize: { width: 400, height: 212 },
          touchPoints: [
            [
              { x: 62.5, y: 92.5 },
              { x: 68.5099, y: 89.7008 },
              { x: 74.8473, y: 86.84 },
              { x: 81.4866, y: 83.9459 },
              { x: 88.402, y: 81.0472 },
              { x: 95.5677, y: 78.1721 },
              { x: 102.9582, y: 75.3491 },
              { x: 110.5475, y: 72.6067 },
              { x: 118.31, y: 69.9733 },
              { x: 126.2199, y: 67.4774 },
              { x: 134.2516, y: 65.1473 },
              { x: 142.3792, y: 63.0116 },
              { x: 150.577, y: 61.0986 },
              { x: 158.8194, y: 59.4369 },
              { x: 167.0805, y: 58.0548 },
              { x: 175.3346, y: 56.9808 },
              { x: 183.556, y: 56.2434 },
              { x: 191.719, y: 55.8709 },
              { x: 199.7978, y: 55.8918 },
              { x: 207.7666, y: 56.3346 },
              { x: 215.5999, y: 57.2277 },
              { x: 223.2717, y: 58.5995 },
              { x: 230.7564, y: 60.4785 },
              { x: 238.0283, y: 62.8931 },
              { x: 245.0616, y: 65.8717 },
              { x: 251.8305, y: 69.4429 },
              { x: 258.3094, y: 73.635 },
              { x: 264.5, y: 78.5 }
            ],
            [
              { x: 335.5, y: 115.8709 },
              { x: 329.4901, y: 118.6701 },
              { x: 323.1527, y: 121.5309 },
              { x: 316.5134, y: 124.4249 },
              { x: 309.598, y: 127.3237 },
              { x: 302.4323, y: 130.1988 },
              { x: 295.0418, y: 133.0218 },
              { x: 287.4525, y: 135.7642 },
              { x: 279.69, y: 138.3976 },
              { x: 271.7801, y: 140.8935 },
              { x: 263.7484, y: 143.2236 },
              { x: 255.6208, y: 145.3593 },
              { x: 247.423, y: 147.2722 },
              { x: 239.1806, y: 148.934 },
              { x: 230.9195, y: 150.3161 },
              { x: 222.6654, y: 151.3901 },
              { x: 214.444, y: 152.1275 },
              { x: 206.281, y: 152.5 },
              { x: 198.2022, y: 152.4791 },
              { x: 190.2334, y: 152.0363 },
              { x: 182.4001, y: 151.1432 },
              { x: 174.7283, y: 149.7714 },
              { x: 167.2436, y: 147.8924 },
              { x: 159.9717, y: 145.4778 },
              { x: 152.9384, y: 142.4991 },
              { x: 146.1695, y: 138.928 },
              { x: 139.6906, y: 134.7358 },
              { x: 133.5, y: 129.8709 }
            ]
          ]
        },
        timeline: [
          {
            // level intro
            start: 4.1,
            finish: 13.001
          },
          {
            // level touch intro
            start: 13.001,
            finish: 13.001
          },
          {
            // level touch
            start: 14,
            finish: 18
          },
          {
            // level win
            start: 18,
            finish: 23.5
          },
          {
            // level fail
            start: 24.5,
            finish: 28
          }
        ],
        restart: 15,
        text: {
          levelTitle: {
            en: 'Level 3',
            de: 'Level 3'
          },
          levelName: {
            en: 'MORNING GLORY',
            de: 'MORNING GLORY'
          },
          levelIntroTitle: {
            en: 'The Situation',
            de: 'Die Situation'
          },
          levelIntroText: {
            en:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.',
            de:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.'
          }
        }
      },
      {
        // Level 4
        drag: {
          tolerance: 30,
          artboardSize: { width: 400, height: 212 },
          touchPoints: [
            [
              { x: 66.5, y: 92.5 },
              { x: 75.7884, y: 87.4894 },
              { x: 85.0364, y: 82.866 },
              { x: 94.2268, y: 78.6314 },
              { x: 103.3423, y: 74.7874 },
              { x: 112.3656, y: 71.3359 },
              { x: 121.2795, y: 68.2786 },
              { x: 130.0667, y: 65.6172 },
              { x: 138.7099, y: 63.3537 },
              { x: 147.1919, y: 61.4896 },
              { x: 155.4953, y: 60.0268 },
              { x: 163.6029, y: 58.9671 },
              { x: 171.4974, y: 58.3122 },
              { x: 179.1616, y: 58.064 },
              { x: 186.5782, y: 58.2242 },
              { x: 193.7298, y: 58.7945 },
              { x: 200.5993, y: 59.7768 },
              { x: 207.1693, y: 61.1727 },
              { x: 213.4226, y: 62.9842 },
              { x: 219.342, y: 65.213 },
              { x: 224.91, y: 67.8607 },
              { x: 230.1095, y: 70.9293 },
              { x: 234.9232, y: 74.4205 },
              { x: 239.3338, y: 78.3361 },
              { x: 243.7921, y: 83.2459 },
              { x: 248.1121, y: 89.3167 },
              { x: 252.0817, y: 96.6855 },
              { x: 255.5, y: 105.5 }
            ],
            [
              { x: 334.5, y: 121.064 },
              { x: 325.2116, y: 126.0746 },
              { x: 315.9636, y: 130.698 },
              { x: 306.7732, y: 134.9326 },
              { x: 297.6577, y: 138.7766 },
              { x: 288.6344, y: 142.2281 },
              { x: 279.7205, y: 145.2854 },
              { x: 270.9333, y: 147.9468 },
              { x: 262.2901, y: 150.2104 },
              { x: 253.8081, y: 152.0744 },
              { x: 245.5047, y: 153.5372 },
              { x: 237.3971, y: 154.5969 },
              { x: 229.5026, y: 155.2518 },
              { x: 221.8384, y: 155.5 },
              { x: 214.4218, y: 155.3398 },
              { x: 207.2702, y: 154.7695 },
              { x: 200.4007, y: 153.7873 },
              { x: 193.8307, y: 152.3913 },
              { x: 187.5774, y: 150.5798 },
              { x: 181.658, y: 148.351 },
              { x: 176.09, y: 145.7033 },
              { x: 170.8905, y: 142.6347 },
              { x: 166.0768, y: 139.1435 },
              { x: 161.6662, y: 135.2279 },
              { x: 157.2079, y: 130.3181 },
              { x: 152.8879, y: 124.2473 },
              { x: 148.9183, y: 116.8785 },
              { x: 145.5, y: 108.064 }
            ]
          ]
        },
        timeline: [
          {
            // level intro
            start: 4.1,
            finish: 13.001
          },
          {
            // level touch intro
            start: 13.001,
            finish: 13.001
          },
          {
            // level touch
            start: 14,
            finish: 18
          },
          {
            // level win
            start: 18,
            finish: 23.5
          },
          {
            // level fail
            start: 24.5,
            finish: 28
          }
        ],
        restart: 15,
        text: {
          levelTitle: {
            en: 'Level 4',
            de: 'Level 4'
          },
          levelName: {
            en: 'MORNING GLORY',
            de: 'MORNING GLORY'
          },
          levelIntroTitle: {
            en: 'The Situation',
            de: 'Die Situation'
          },
          levelIntroText: {
            en:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.',
            de:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.'
          }
        }
      },
      {
        // Level 5
        drag: {
          tolerance: 30,
          artboardSize: { width: 400, height: 212 },
          touchPoints: [
            [{ x: 66, y: 93 }, { x: 164, y: 59 }, { x: 255, y: 66 }],
            [{ x: 331, y: 122 }, { x: 237, y: 155 }, { x: 145, y: 146 }]
          ]
        },
        timeline: [
          {
            // level intro
            start: 4.1,
            finish: 13.001
          },
          {
            // level touch intro
            start: 13.001,
            finish: 13.001
          },
          {
            // level touch
            start: 14,
            finish: 18
          },
          {
            // level win
            start: 18,
            finish: 23.5
          },
          {
            // level fail
            start: 24.5,
            finish: 28
          }
        ],
        restart: 15,
        text: {
          levelTitle: {
            en: 'Level 5',
            de: 'Level 5'
          },
          levelName: {
            en: 'MORNING GLORY',
            de: 'MORNING GLORY'
          },
          levelIntroTitle: {
            en: 'The Situation',
            de: 'Die Situation'
          },
          levelIntroText: {
            en:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.',
            de:
              'Sie ist wach – aber dein Arm ist eingeschlafen. Öffne ihren Bra einhändig. Das schaffst du doch mit links.'
          }
        }
      }
    ]
  },
  audioFiles: ['stretch', 'tap-resonant', 'ticktack', 'nodear', 'ohyeh', 'loop'],
  videoFiles: [
    'https://player.vimeo.com/external/249370124.hd.mp4?s=515c32a3b54c7248adc5910ac970d9382df9cd0c&profile_id=174',
    'https://player.vimeo.com/external/251341572.hd.mp4?s=866d2cb4b7dca6bbfca421535e1e6e2e04e8cf5a&profile_id=174'
    // 'https://player.vimeo.com/external/112266799.hd.mp4?s=0b1320737d5aef00fd3ba521a6b2f3ad'
  ]
};
