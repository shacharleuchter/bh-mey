import * as createjs from 'createjs-module';
import { TimelineMax, Power2, Sine } from 'gsap';

const Debug = {
  drawCenter: false,
  drawTolerence: false
};

const BLURRY_OFFSET_MAX_X = 20;
const BLURRY_OFFSET_MAX_Y = 80;

const DOT_MAX_SIZE = 3;
const DOT_NORMAL_SIZE = 1;
const DOT_NORMAL_SCALE = DOT_NORMAL_SIZE / DOT_MAX_SIZE;

const TOUCH_STROKE_WIDTH = 2;

class TouchDrawing extends createjs.Container {
  constructor() {
    super();

    this.tolerance = 0;
    this.dots = [];

    this.targetPathsContainer = new createjs.Container();
    this.addChild(this.targetPathsContainer);

    this.touchPathsShape = new createjs.Shape();
    this.addChild(this.touchPathsShape);

    if (Debug.drawCenter) {
      const ctrl = new createjs.Shape(new createjs.Graphics().beginFill('yellow').drawCircle(0, 0, 2));
      this.addChild(ctrl);
    }
  }

  randomizePoint(point, valueX = BLURRY_OFFSET_MAX_X, valueY = BLURRY_OFFSET_MAX_Y) {
    if (point) {
      point = {
        x: point.x + Math.random() * valueX - valueX / 2,
        y: point.y + Math.random() * valueX - valueX / 2
      };
    }
    return point;
  }

  reset() {
    this.touchPathsShape.graphics.clear();
  }

  prepareStartAnimation() {}

  animateStart() {
    TweenMax.to(this.targetPathsTimeline, 1, { progress: 1, ease: Power2.easeOut });
  }

  drawTargetPaths(paths, blurryLines = 0) {
    // Draw tolerance area (for debugging/testing)
    if (Debug.drawTolerence) {
      if (!this.debugToleranceCanvas) {
        this.debugToleranceCanvas = new createjs.Shape();
        this.addChild(this.debugToleranceCanvas);
      }
      const debugToleranceCanvasGraphics = this.debugToleranceCanvas.graphics;
      debugToleranceCanvasGraphics.clear();
      debugToleranceCanvasGraphics.setStrokeStyle(this.tolerance, 'round');
      debugToleranceCanvasGraphics.beginStroke('white');

      paths.forEach(path => {
        if (path && path.length) {
          let p1 = path[0];
          let p2 = path.length >= 1 ? path[1] : path[0];
          debugToleranceCanvasGraphics.moveTo(p1.x, p1.y);

          for (let i = 1, numPaths = path.length; i < numPaths; i++) {
            const ctrlPoint = this.getControlPoint(p1, p2);
            debugToleranceCanvasGraphics.quadraticCurveTo(p1.x, p1.y, ctrlPoint.x, ctrlPoint.y);
            p1 = path[i];
            p2 = path[i + 1];
          }
          debugToleranceCanvasGraphics.lineTo(p1.x, p1.y);
        }
      });

      this.debugToleranceCanvas.alpha = 0.1;
    }

    // Draw dots along target paths
    const timeline = new TimelineMax();
    timeline.pause();
    paths.forEach(path => {
      const pathDots = [];
      const pathTimeline = new TimelineMax();
      if (path && path.length) {
        path.forEach((point, pointIndex) => {
          const dot = new createjs.Shape();
          dot.x = point.x;
          dot.y = point.y;
          dot.scaleX = dot.scaleY = 0;
          dot.alpha = 0;
          const g = dot.graphics;
          g.beginFill('white');
          g.drawCircle(0, 0, DOT_MAX_SIZE);
          this.targetPathsContainer.addChild(dot);
          pathDots.push(dot);

          pathTimeline.to(dot, 1, { scaleX: 1, scaleY: 1, alpha: 1, ease: Sine.easeIn }, pointIndex * 0.25);
          pathTimeline.to(dot, 0.25, { scaleX: DOT_NORMAL_SCALE, scaleY: DOT_NORMAL_SCALE, ease: Sine.easeOut });
        });
      }
      timeline.add(pathTimeline, 0);
      this.dots.push(pathDots);
    });
    this.targetPathsTimeline = timeline;
  }

  drawTouchPaths(paths) {
    const g = this.touchPathsShape.graphics;
    g.clear();
    g.setStrokeStyle(TOUCH_STROKE_WIDTH);
    g.beginStroke('rgba(255,255,255, 1)');

    paths.forEach(path => {
      if (path && path.length) {
        let p1 = path[0];
        let p2 = path.length >= 1 ? path[1] : path[0];
        g.moveTo(p1.x, p1.y);

        for (let i = 1, numPaths = path.length; i < numPaths; i++) {
          var ctrlPoint = this.getControlPoint(p1, p2);
          g.quadraticCurveTo(p1.x, p1.y, ctrlPoint.x, ctrlPoint.y);
          p1 = path[i];
          p2 = path[i + 1];
        }
        g.lineTo(p1.x, p1.y);
      }
    });
  }

  getControlPoint(p1, p2) {
    return {
      x: p1.x + (p2.x - p1.x) / 2,
      y: p1.y + (p2.y - p1.y) / 2
    };
  }
}
export default createjs.promote(TouchDrawing, 'Container');
