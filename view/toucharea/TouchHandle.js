import * as createjs from 'createjs-module';
import MiniSignal from 'mini-signals';
import { TimelineMax, TweenMax, Back, Sine } from 'gsap';

const Debug = {
  drawHitArea: false
};

let counter = -1;

const hitAreaSize = 64;
const originSize = 46;
const hoverSize = 64;
const hoverScale = hoverSize / originSize;
const arrowWidth = 16;
const arrowHeight = 24;
const arrowScale = 0.5;

class TouchHandle extends createjs.Container {
  constructor(originPoint, container) {
    super();

    this.dragPath = [];
    this.draggingEnabled = true;

    this.container = container;
    this.container.addChild(this);

    // set props
    this.x = this.originX = originPoint.x;
    this.y = this.originY = originPoint.y;
    this.id = ++counter;

    // Init component
    this.circle = new createjs.Container();
    this.addChild(this.circle);

    this.circleFill = new createjs.Shape();
    // this.circleFill.graphics.beginFill('#000').drawCircle(0, 0, originSize / 2);
    this.circle.addChild(this.circleFill);

    this.circleStroke = new createjs.Shape();
    this.circleStroke.graphics
      .setStrokeStyle(2, 'round')
      .beginStroke('rgba(255, 255, 255, 0.4)')
      .drawCircle(0, 0, originSize / 2);
    this.circle.addChild(this.circleStroke);

    this.arrow = new createjs.Bitmap('/images/chevron-right.png');
    this.arrow.regX = arrowWidth * arrowScale;
    this.arrow.regY = arrowHeight * arrowScale;
    this.arrow.x += 1; // correct visual center
    this.arrow.scaleX = this.arrow.scaleY = arrowScale;
    this.circle.addChild(this.arrow);

    // Define hitArea used for touch events
    const hitArea = new createjs.Shape();
    hitArea.graphics.beginFill('#000000').drawCircle(0, 0, hitAreaSize / 2);
    this.hitArea = hitArea;

    if (Debug.drawHitArea) {
      const hitAreaVisible = new createjs.Shape();
      hitAreaVisible.graphics.beginFill('rgba(255,255,0, 0.25)').drawCircle(0, 0, hitAreaSize / 2);
      this.addChild(hitAreaVisible);
    }

    // Events
    this.mouseChildren = false;
    this.on('pressmove', evt => this.onPressmove(evt));
    this.on('pressup', evt => this.onPressup(evt));
    this.on('mousedown', evt => this.onMousedown(evt));

    // Signals
    this.dragged = new MiniSignal();
    this.dragEnded = new MiniSignal();
    this.dragStarted = new MiniSignal();
    this.startAnimationComplete = new MiniSignal();
  }

  reset(rotation) {
    [this, this.circleFill, this.circleStroke, this.arrow].forEach(obj => {
      TweenMax.killTweensOf(obj);
      obj.alpha = 1;
    });

    this.x = this.originX;
    this.y = this.originY;
    this.scaleX = this.scaleY = 1;
    this.rotation = rotation;

    this.clearPath();
    this.draggingEnabled = true;
  }

  animateToTarget(point) {
    const timeline = new TimelineMax()
      .to(this, 0.25, {
        x: point.x,
        y: point.y,
        ease: Back.easeOut
      })
      .to(
        [this.circleFill, this.circleStroke],
        0.25,
        {
          alpha: 1
        },
        0
      )
      .to(this, 0.15, {
        scaleX: 0.8,
        scaleY: 0.8,
        ease: Sine.easeIn
      })
      .to(this, 0.15, {
        scaleX: 1,
        scaleY: 1,
        ease: Sine.easeOut
      });
  }

  animateToOrigin() {
    TweenMax.to(this, 0.35, {
      x: this.originX,
      y: this.originY,
      scaleX: 1,
      scaleY: 1,
      ease: Back.easeOut
    });

    TweenMax.to([this.circleFill, this.circleStroke, this.arrow], 0.25, { alpha: 1 });
  }

  animateToStart(startX, startY) {
    [this, this.circleFill, this.circleStroke, this.arrow].forEach(obj => {
      TweenMax.killTweensOf(obj);
      obj.alpha = 1;
    });

    this.arrow.alpha = 0;
    this.x = startX;
    this.y = startY;

    const timeline = new TimelineMax({
      delay: 0.1,
      onComplete: () => {
        this.startAnimationComplete.dispatch();
      }
    })
      .to(this, 0.6, {
        x: this.originX,
        y: this.originY,
        ease: Power2.easeInOut
        // ease: CustomEase.create('custom', 'M0,0 C0.322,0 0.525,0.981 0.726,1.024 0.82,1.044 0.838,1 1,1')
      })
      .to(this.arrow, 0.35, { alpha: 1 });
  }

  clearPath() {
    this.dragPath = [];
  }

  onMousedown(evt) {
    if (!this.draggingEnabled) {
      return;
    }

    TweenMax.to(this, 0.25, {
      scaleX: hoverScale,
      scaleY: hoverScale,
      ease: Back.easeOut
    });

    TweenMax.to([this.circleFill, this.arrow], 0.25, {
      alpha: 0
    });

    TweenMax.to(this.circleStroke, 0.25, {
      alpha: 0.25
    });

    this.dragStarted.dispatch();
  }

  onPressup(evt) {
    this.dragEnded.dispatch();
  }

  onPressmove(evt) {
    if (!this.draggingEnabled) {
      return;
    }

    this.drag(this.container.globalToLocal(evt.stageX, evt.stageY));
  }

  drag(pos) {
    this.x = pos.x;
    this.y = pos.y;
    this.dragPath.push(pos);
    this.dragged.dispatch();
  }
}
export default createjs.promote(TouchHandle, 'Container');
