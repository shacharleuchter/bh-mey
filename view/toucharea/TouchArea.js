import * as createjs from 'createjs-module';
import TouchAreaStage from './TouchAreaStage';
import TouchHandle from './TouchHandle';
import TouchDrawing from './TouchDrawing';
import MiniSignal from 'mini-signals';
import { distance, angleBetweenDegrees } from './Utils';

export default class TouchArea {
  constructor(canvasElement, offsetY = 0) {
    // Set a high framerate to get smooth movements
    createjs.Ticker.framerate = 60;

    // Signals
    // `handleDragEnded` will be dispatched, when the dragging ended,
    // either on touchend or because the user successfully
    // dragged both handles along the given path.
    this.handleDragEnded = new MiniSignal();
    this.handleDragged = new MiniSignal();
    this.handleDragStarted = new MiniSignal();

    // Level successfully completed?
    this.success = false;

    // Defines the progress of the current level
    // progress=0 => The user did not dragged at all
    // progress=1 => The user has traced the path completely
    this.progress = 0;

    // Build stage
    this.stage = new TouchAreaStage(canvasElement, offsetY);
    this.touchDrawing = new TouchDrawing();
    this.container.addChild(this.touchDrawing);

    // Add handles
    this.handles = [
      this.createHandle({ x: -165, y: 0 }, this.container),
      this.createHandle({ x: 165, y: 0 }, this.container)
    ];

    // Init update interval
    createjs.Ticker.addEventListener('tick', evt => this.handleTick(evt));
  }

  /**
   * Start a new or current (if dragOptions==false/undefined) level
   *
   * @param {array} dragOptions
   */
  startLevel(dragOptions, animatePaths = true, animateHandles = false, startBtnCenterX = null, startBtnCenterY = null) {
    console.log('TouchArea: Start level, dragOptions', dragOptions);

    // Store dragOptions if defined
    if (dragOptions) {
      this.dragOptions = dragOptions;

      // Translate touchPoints within the Illustrator artboard context
      // to the TouchArea context
      const artboardCenterX = this.dragOptions.artboardSize.width / 2;
      const artboardCenterY = this.dragOptions.artboardSize.height / 2;
      const translatedTouchPoints = [];
      this.dragOptions.touchPoints.forEach(points => {
        const translatedPoints = [];
        points.forEach(point => {
          translatedPoints.push({
            x: point.x - artboardCenterX,
            y: point.y - artboardCenterY
          });
        });
        translatedTouchPoints.push(translatedPoints);
      });

      this.targetPaths = translatedTouchPoints;
      this.tolerance = this.dragOptions.tolerance;
    }

    this.touchDrawing.reset();
    this.touchDrawing.tolerance = this.dragOptions.tolerance;
    this.touchDrawing.drawTargetPaths(this.targetPaths, this.dragOptions.blurryLines);

    this.touchDrawing.prepareStartAnimation();
    this.handles.forEach((handle, handleIndex) => {
      // Compute rotation to point handle towards first touch point
      const firstTouchPoint = this.targetPaths[handleIndex][0];
      const rotation = angleBetweenDegrees(handle.originX, handle.originY, firstTouchPoint.x, firstTouchPoint.y);
      handle.reset(rotation);

      if (startBtnCenterX && startBtnCenterY) {
        const handleStartX = startBtnCenterX - this.container.x;
        const handleStartY = startBtnCenterY - this.container.y;
        handle.animateToStart(handleStartX, handleStartY);
      }

      if (animatePaths && handleIndex === 0) {
        if (animateHandles) {
          this.startAnimationCompleteBinding = handle.startAnimationComplete.once(() => {
            this.touchDrawing.animateStart();
          });
        } else {
          this.touchDrawing.animateStart();
        }
      }
    });

    this.progress = 0;
    this.success = false;
  }

  createHandle(startPoint, container) {
    const handle = new TouchHandle(startPoint, container);

    handle.dragged.add(() => {
      this.drawTouchPaths();
      this.checkHandles();
      this.handleDragged.dispatch();
    });

    handle.dragEnded.add(() => {
      this.checkHandles();
      if (!this.success) {
        this.handles.forEach(handle => {
          handle.draggingEnabled = true;
          handle.clearPath();
          handle.animateToOrigin();
        });
        this.handleDragEnded.dispatch(false);
      }
      this.drawTouchPaths();
    });

    handle.dragStarted.add(() => {
      this.handleDragStarted.dispatch();
    });

    return handle;
  }

  getXOfClosestTouchPoint(points, multiplier) {
    return points.map(point => point.x * multiplier).reduce((pre, cur) => Math.max(pre, cur));
  }

  checkHandles() {
    if (this.success) {
      return true;
    }

    let progress = 0;
    this.handles.forEach((handle, handleIndex) => {
      let progressByHandle = 0;
      if (handle.dragPath.length > 0) {
        const direction = handleIndex === 0 ? 1 : -1;
        const targetPath = this.targetPaths[handleIndex];
        const lastTargetPointX = targetPath[targetPath.length - 1].x;
        const closestTouchPointX = this.getXOfClosestTouchPoint(handle.dragPath, direction);
        const touchDist = Math.abs(closestTouchPointX - handle.originX * direction);
        const maxDist = Math.abs(lastTargetPointX - handle.originX);
        progressByHandle = touchDist / maxDist;
      }
      progress += progressByHandle;
    });
    this.progress = Math.max(0, Math.min(progress / 2, 1));

    const hits = [];
    this.handles.forEach((handle, handleIndex) => {
      const targetPath = this.targetPaths[handleIndex];
      const touchPath = handle.dragPath;
      let touchPointIndex = 0;
      let checkNextTargetPoint = true;
      hits[handleIndex] = 0;
      for (let i = 0, numTargetPoints = targetPath.length; i < numTargetPoints; i++) {
        const targetPoint = targetPath[i];
        for (let j = touchPointIndex, numTouchPoints = touchPath.length; j < numTouchPoints; j++) {
          const touchPoint = touchPath[j];
          const dist = distance(targetPoint, touchPoint);
          if (dist < this.tolerance) {
            touchPointIndex = j;
            ++hits[handleIndex];
            break;
          }
          if (j === numTouchPoints - 1) {
            checkNextTargetPoint = false;
          }
        }
        if (!checkNextTargetPoint) {
          //console.log(`TouchArea: Break checking target points at ${i}`);
          break;
        }
      }
    });

    let hitAllTargetPoints = true;
    hits.forEach((hit, hitIndex) => {
      if (hitAllTargetPoints && hit != this.targetPaths[hitIndex].length) {
        hitAllTargetPoints = false;
      }
    });
    this.success = hitAllTargetPoints;

    if (this.success) {
      this.progress = 1;
      this.handles.forEach((handle, handleIndex) => {
        const targetPath = this.targetPaths[handleIndex];
        handle.draggingEnabled = false;
        handle.animateToTarget(targetPath[targetPath.length - 1]);
      });
      this.handleDragEnded.dispatch(true);
    }

    return this.success;
  }

  drawTouchPaths() {
    let paths = [];
    this.handles.forEach(handle => {
      paths.push(handle.dragPath);
    });
    this.touchDrawing.drawTouchPaths(paths);
  }

  handleTick(evt) {
    this.stage.update();
  }

  get container() {
    return this.stage ? this.stage.container : undefined;
  }
}
