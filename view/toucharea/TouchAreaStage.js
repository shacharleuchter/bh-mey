import * as createjs from 'createjs-module';
import backingScale from './BackingScale';

class TouchStage extends createjs.Stage {
  constructor(canvasId, offsetY) {
    // invoke Container constructor
    super(canvasId);

    this.offsetY = offsetY;

    // Keep tracking the mouse even when it leaves the canvas
    this.mouseMoveOutside = true;
    // Enable touch events (and thereby prevent scrolling of body)
    createjs.Touch.enable(this, false, true);

    // Add container, which will contain all display objects
    // and will be centered.
    this.container = new createjs.Container();
    this.addChild(this.container);

    // Scale canvas depending on backing scale and viewport size
    // to appear crisp on Retina
    console.log('TouchArea: Backing Scale', backingScale);
    this.scaleX = backingScale;
    this.scaleY = backingScale;
    window.addEventListener('resize', () => this.resize(), false);
    window.addEventListener('orientationchange', evt => this.orientationchange(evt), true);
    this.resize();
  }

  orientationchange(evt) {
    console.log('orientationchange', evt);
    setTimeout(() => this.resize(true), 1000);
  }

  resize(orientationchanged) {
    console.log(
      `TouchArea: Window Resized; orientationchanged=${orientationchanged}, width=${this.width}, height=${this.height}`
    );

    // Resize canvas and
    this.canvas.width = this.width * this.scaleX;
    this.canvas.height = this.height * this.scaleY;
    this.canvas.style.width = `${this.width}px`;
    this.canvas.style.height = `${this.height}px`;

    // Center container
    this.container.x = this.width / 2;
    this.container.y = Math.ceil(this.height / 2 + this.offsetY);
  }

  // TODO: Limit to max-width/max-height for canvas elements depending on device.
  get width() {
    return Math.min(window.innerWidth, 4096);
  }

  get height() {
    return Math.min(window.innerHeight, 4096);
  }
}
export default createjs.promote(TouchStage, 'Stage');
